
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

<?php include('parts/head.php'); ?>

<body id="bienvenido" <?php body_class(); ?>>

	<?php include('parts/row1.php'); ?>
			<div class="row-fluid row_2 background-red-dark">
				<div class="container banner color-white">
					
					<div class="col-lg-12">
						<div class="page-header">
							<h2 class=""><?php the_title(); ?></h2>
						</div>
						<!-- Imágen banner -->
						<div class="row">
						<div class="col-md-4 col-sm-12 col-xs-12 col-lg-4">
							<div class="row">
			    				<div class="col-xs-6 col-md-12 col-lg-12 col-xs-offset-4 col-lg-offset-1 col-md-offset-1 ">
									<div class="row">
										<a href="<?php the_permalink(); ?>" class="title-page">
										<?php  $default_attr = array('alt'	=> 'texto para mostrar en Alt',
													'title'	=> 'texto para mostrar en Title',
													'class' => 'img-responsive'
												);
											$size='full';
										 the_post_thumbnail( $size, $default_attr);
										?>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-8 col-sm-12 col-xs-12 col-lg-8">
							<!-- Titulo de banner -->
							
							<!-- Contenido de banner -->
							<div class="contenido-banner">
								<?php if(have_posts()): ?>
								<?php while(have_posts() ): the_post(); $i++; ?>
									<?php the_content();?>
								<?php endwhile; ?>
								<?php endif; ?>
							</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Row 2 -->
			<!-- End Row 5 [Proyectos] -->
			<div id="proyectos" class="row-fluid row_5">

				<!-- title projects -->
				<div class="background-blue-dark">
					<div class="container title-projects text-center color-white">
						<h3><?php echo "OTROS ".$options['titulo-proyectos']; ?></h3>
					</div>
				</div>

				<div class="container projects">

					<!-- Obtiene los últimos 7 trabajos del portafolio -->			
					<?php get_portfolio(); ?>

				</div>
			</div>
			<!-- End Row 5 -->

	<?php include('parts/row6.php'); ?>
	<?php include('parts/row7.php'); ?>
</body>
</html>
