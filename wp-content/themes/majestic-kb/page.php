<!-- Get Header -->
<?php get_header(); ?>
	
	<!-- Star row4 (Content Main) -->
	<div class="row4">
		<div class="container">
			<div class="col-md-12">
			<!-- Start Page -->
				<div class="page">

					<?php if(have_posts()): ?>
						
						<!-- Start Loop Get Page -->
						<?php while(have_posts() ): the_post(); ?>

						<!-- Title Page-->
						<div class="pageTitle">
							<h1><?php the_title(); ?></h1>
						</div>

						<!-- Image Page-->
						<div class="pageImage">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('medium'); ?></a>
						</div>

						<!-- Content Page-->
						<div class="pageContent">
							<div class="row" >
							<?php the_content(); ?>
							</div>
						</div>

						<?php endwhile; ?>
						<!-- End Loop Get Page -->

					<?php else: ?>
						<p><?php _e("PAGE NOT FOUND"); ?></p>
					<?php endif; ?>

				</div>
			</div>
			<!-- End Page -->

		</div>
	</div>
	<!-- End row4 (Content Main) -->
	
<!-- Get Footer -->
<?php get_footer(); ?>