<?php
	function optionsframework_option_name()
	{
		return 'options-framework-theme';
	}

	function optionsframework_options()
	{
		// Pull all the pages into an array
		$options_pages 		= array();
		$options_pages_obj 	= get_pages( 'sort_column=post_parent,menu_order' );
		$options_pages[''] 	= 'Select a page:';
		
		foreach ($options_pages_obj as $page)
		{
			$options_pages[$page->ID] = $page->post_title;
		}

		// If using image radio buttons, define a directory path
		$imagepath =  get_template_directory_uri() . '/images/';

		$options = array();


			/* Start opciones Branding */
			$options[] = array(
				'name' => __( 'Branding', 'theme-textdomain' ),
				'type' => 'heading'
			);
				/* Personalizar logo */
				$options[] = array(
					'name' 			=> __( 'Agregar Logo', 'theme-textdomain' ),
					'desc' 			=> __( 'Para una buena relacion de aspecto la imagen debe ser proporcional 
											entre 350 X 46 pixeles.', 'theme-textdomain' ),
					'id' 			=> 'logo',
					'type' 			=> 'upload'
				);

				/* Personalizar favicon */
				$options[] = array(
					'name' 			=> __( 'Agregar Favicon', 'theme-textdomain' ),
					'desc' 			=> __( 'Para una buena relacion de aspecto la imagen debe ser proporcional 
											entre 50 X 50 pixeles.', 'theme-textdomain' ),
					'id' 			=> 'favicon',
					'type' 			=> 'upload'
				);

				/* Personalizar titulo */
				$options[] = array(
					'name' 			=> __( 'Titulo Web'),
					'desc' 			=> __( 'Este titulo es interpretado por el navegador'),
					'id' 			=> 'titulo',
					'placeholder' 	=> 'Titulo Web',
					'std' 			=> 'Majestic KB',
					'type' 			=> 'text'
				);

				/* Personalizar teléfono */
				$options[] = array(
					'name' 			=> __( 'Dirección Label'),
					'desc' 			=> __( 'Este es la dirección de la compañía'),
					'id' 			=> 'direccion',
					'placeholder' 	=> 'Dirección',
					'std' 			=> '4992 W Atlantic Blvd Margate, FL 33063',
					'type' 			=> 'text'
				);
				$options[] = array(
					'name' 			=> __( 'Teléfono Label'),
					'desc' 			=> __( 'Este es el teléfono de la compañía'),
					'id' 			=> 'telefono',
					'placeholder' 	=> 'Teléfono',
					'std' 			=> '954 532 4239',
					'type' 			=> 'text'
				);
				$options[] = array(
					'name' 			=> __( 'Teléfono celular Label'),
					'desc' 			=> __( 'Este es el teléfono celular de la compañía'),
					'id' 			=> 'celular',
					'placeholder' 	=> 'Celular',
					'std' 			=> '754 246 0626',
					'type' 			=> 'text'
				);
				$options[] = array(
					'name' 			=> __( 'Horario Label'),
					'desc' 			=> __( 'Este es el horario de la compañía'),
					'id' 			=> 'horario',
					'placeholder' 	=> 'Horario',
					'std' 			=> '',
					'type' 			=> 'textarea'
				);
				$options[] = array(
					'name' 			=> __( 'Teléfono Link'),
					'desc' 			=> __( 'Este es el link que se activará al presionar el teléfono'),
					'id' 			=> 'telefono-link',
					'placeholder' 	=> 'Teléfono Link',
					'std' 			=> '+17863554444',
					'type' 			=> 'text'
				);
				$options[] = array(
					'name' 			=> __( 'Email'),
					'desc' 			=> __( 'Este es el Email de la compañía'),
					'id' 			=> 'email-link',
					'placeholder' 	=> 'Email Link',
					'std' 			=> 'info@onefocusdigital.com',
					'type' 			=> 'text'
				);
				$options[] = array(
					'name' 			=> __( 'Instagram'),
					'desc' 			=> __( 'Este es el Instagram de la compañía'),
					'id' 			=> 'instagram-link',
					'placeholder' 	=> 'Instagram Link',
					'std' 			=> '@MajesticKB',
					'type' 			=> 'text'
				);
				$options[] = array(
					'name' 			=> __( 'Facebook'),
					'desc' 			=> __( 'Este es el Facebook de la compañía'),
					'id' 			=> 'facebook-link',
					'placeholder' 	=> 'Facebook Link',
					'std' 			=> 'MajesticKB',
					'type' 			=> 'text'
				);
				
				/* Personalizar background footer */
				$options[] = array(
					'name' 			=> __( 'Agregar background footer', 'theme-textdomain' ),
					'desc' 			=> __( 'Para una buena relacion de aspecto la imagen debe ser proporcional 
											entre 350 X 46 pixeles.', 'theme-textdomain' ),
					'id' 			=> 'background-footer',
					'type' 			=> 'upload'
				);

				/* Personalizar titulo */
				$options[] = array(
					'name' 			=> __( 'Drechos de autor'),
					'desc' 			=> __( 'Este campos se utiliza en el footer'),
					'id' 			=> 'copyright',
					'placeholder' 	=> 'Derechos de Autor',
					'std' 			=> 'Copyright 2016© All Rights Reserved www.majestic.com',
					'type' 			=> 'text'
				);
			/* End opciones Branding */

			
			/* Start opciones Slider */
			$options[] = array(
				'name' 				=> __( 'Slider', 'theme-textdomain' ),
				'type' 				=> 'heading'
			);
				/* Personalizar slider 1 */
				$options[] = array(
					'name' 			=> __( 'Slider Imágen 1', 'theme-textdomain' ),
					'desc' 			=> __( 'Para una buena relacion de aspecto la imagen debe ser proporcional 
											entre 1140 X 500 pixeles.', 'theme-textdomain' ),
					'id' 			=> 'slider1',
					'type' 			=> 'upload'
				);
				/* Personalizar slider 2 */
				$options[] = array(
					'name' 			=> __( 'Slider Imágen 2', 'theme-textdomain' ),
					'desc' 			=> __( 'Para una buena relacion de aspecto la imagen debe ser proporcional 
											entre 1140 X 500 pixeles.', 'theme-textdomain' ),
					'id' 			=> 'slider2',
					'type' 			=> 'upload'
				);
				/* Personalizar slider 3 */
				$options[] = array(
					'name' 			=> __( 'Slider Imágen 3', 'theme-textdomain' ),
					'desc' 			=> __( 'Para una buena relacion de aspecto la imagen debe ser proporcional 
											entre 1140 X 500 pixeles.', 'theme-textdomain' ),
					'id' 			=> 'slider3',
					'type' 			=> 'upload'
				);
				/* Personalizar slider 4 */
				$options[] = array(
					'name' 			=> __( 'Slider Imágen 4', 'theme-textdomain' ),
					'desc' 			=> __( 'Para una buena relacion de aspecto la imagen debe ser proporcional 
											entre 1140 X 500 pixeles.', 'theme-textdomain' ),
					'id' 			=> 'slider4',
					'type' 			=> 'upload'
				);
				/* Personalizar slider 5 */
				$options[] = array(
					'name' 			=> __( 'Slider Imágen 5', 'theme-textdomain' ),
					'desc' 			=> __( 'Para una buena relacion de aspecto la imagen debe ser proporcional 
											entre 1140 X 500 pixeles.', 'theme-textdomain' ),
					'id' 			=> 'slider5',
					'type' 			=> 'upload'
				);
			/* End opciones Slider */



			/* Start opciones Banner */
			$options[] = array(
				'name' 				=> __( 'Contenido', 'theme-textdomain' ),
				'type' 				=> 'heading'
			);	
				/* Personalizar contenido del SEO */
				$options[] = array(
					'name' 			=> __( 'Contenido SEO Index Párrafo 1'),
					'desc' 			=> __( 'Este es un contenido para SEO ubicado en el Index'),
					'id' 			=> 'contenido-index-p1',
					'placeholder' 	=> 'Contenido SEO Index',
					'std' 			=> 'Contenido para SEO.',
					'type' 			=> 'editor'
				);

				/* Personalizar contenido del SEO */
				$options[] = array(
					'name' 			=> __( 'Contenido SEO Index Párrafo 2'),
					'desc' 			=> __( 'Este es un contenido para SEO ubicado en el Index'),
					'id' 			=> 'contenido-index-p2',
					'placeholder' 	=> 'Contenido SEO Index',
					'std' 			=> 'Contenido para SEO.',
					'type' 			=> 'editor'
				);
				
				/* End opciones Bienvenido */
			/* End opciones Banner */

		return $options;
	}