<?php $options = get_options(); ?>

	<!-- Start row5 (Contact And Map) -->
	<div class="row5">
		<div class="container">
				<div class="col-md-12">
				 	<div class="footer-img" style="background-image: url('<?php echo $options['background-footer']; ?>');">
						<div class="col-sm-6 col-md-6 col-lg-6 information-footer">
							<h1 class="titleRow">
								MAJESTIC KITCHENS & BATHS
							</h1>
							<p>
								MKB focuses on home renovations such as kitchens, bathrooms and flooring. We also offer services for comercial purposes.
							</p>

							<!-- Socials Icons -->
							<div class="socials">
								<a href="<?php echo $options['instagram-link']; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a>
								<a href="<?php echo $options['facebook-link']; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
							</div>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-6 information-footer hidden-xs">
							<div class="contact-information">
								<span>
									<i class="fa fa-map-marker" aria-hidden="true"></i> <br>
									<?php echo $options['direccion']; ?>
								</span>
							</div>
							<div class="contact-information">
								<span>
									<i class="fa fa-phone" aria-hidden="true"></i>  <br>
									<?php echo $options['telefono']; ?>
								</span>
							</div>
							<div class="contact-information">
								<span>
									<i class="fa fa-mobile" aria-hidden="true"></i>  <br>
									<?php echo $options['celular']; ?>
								</span>
							</div>
							<div class="contact-information">
								<span>
									<i class="fa fa-envelope-o" aria-hidden="true"></i> <br>
									<?php echo $options['email-link']; ?>
								</span>
							</div>
							<div class="contact-information">
								<span>
									<i class="fa fa-clock-o" aria-hidden="true"></i> <br>
									<?php echo $options['horario']; ?>
								</span>
							</div>
						</div>
					</div>
				</div>
		</div>
	</div>
	<!-- End row5 (Content Main) -->

	<!-- Star row6 (Footer) -->
	<div class="row6">
		<div class="container">
			<div class="col-md-6 footer footer-left"><?php echo $options['copyright']; ?></div>

			<!-- Socials Icons -->
			<div class="col-md-6 footer footer-right">
				<a href="<?php echo $options['instagram-link']; ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
				<a href="<?php echo $options['facebook-link']; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
			</div>
			<!-- End Socials Icons -->
			</div>
		</div>
	</div>
	<!-- End row6 (Footer) -->


	<!-- JS Jquery -->
	<script src="<?php echo JS ?>/jquery-1.12.3.min.js"></script>
	
	<!-- JS Bootstrap -->
	<script src="<?php echo JS ?>/bootstrap.min.js"></script>


	<script>
		$('.dropdown-toggle').on('click',function(){
			$('.navbar-nav li .dropdown-toggle').each(function(){
			$(this).removeClass('active');
			/*$('.navbar-nav li').removeClass('active');*/

			});
			$(this).addClass('active')
		});
		$('#top-link-block').click(function(){
		    $("html, body").animate({ scrollTop: 0 }, 700);
		    return false;
		});
		$('#submit').addClass('btn btn-theme');
		$('.wpcr3_button_1').addClass('btn btn-theme');
		$('.wpcr3_button_1').removeClass('wpcr3_button_1');
		$('.wpcr3_in_content').attr('style', 'margin: 0px 15px !important; font-size: 25px !important;');
		$('.wpcr3_power').attr('style', 'display:none !important;');
		$('blockquote').attr('style', 'border-left: 5px solid rgb(250, 212, 53);;');
		$('.wpcr3_review_title').attr('style', 'margin-bottom:16px;');
	</script>
</body>
</html>