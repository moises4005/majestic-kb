<?php
	/*
	* 	Support Theme Options
	*/
	function get_options()
	{
		if ( !function_exists( 'optionsframework_init' ) ) {
			define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
			require_once dirname( __FILE__ ) . '/inc/options-framework.php';
		}
		$options = get_option('options-framework-theme');
		return $options;
	}
	get_options();

	/*
	* 	Defines the path to theme
	*/
	define('TEMPPATH' , get_bloginfo('stylesheet_directory'));

	/*
	* 	Defines the path to IMAGES
	*/
	define('IMAGES' , TEMPPATH. "/assets/images");

	/*
	* 	Defines the path to CSS
	*/
	define('CSS' , TEMPPATH. "/assets/css");

	/*
	* 	Defines the path to JS
	*/
	define('JS' , TEMPPATH. "/assets/js");

	/*
	* 	Defines support navs
	*/
	register_nav_menus( array(
	    'primary' => __( 'Primary Menu', 'THEMENAME' ),
	) );

	/*
	* 	Support for content images thumbnails
	*/
	add_theme_support( 'post-thumbnails' );
?>