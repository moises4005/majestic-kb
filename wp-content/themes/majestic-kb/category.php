<!-- Get Header -->
<?php get_header(); ?>
<?php
/*
Template Name: Blog
*/
?>	

	<!-- Star row4 (Content Main) -->
	<div class="row4">
		<div class="container">

			<!-- Start Page -->
			<div class="page">

				<!-- Start Posts -->
				<?php $posts= get_posts(); ?>
				<?php if ($posts) : ?>
					<?php while(have_posts()): the_post(); $i++; ?>


						<!-- Set data post -->
						<?php setup_postdata($post); ?>

						<!-- Start Posts -->
						<div class="col-md-4">
							<div class="post">

								<!-- Image Post-->
								<div class="postImage">
									<?php the_post_thumbnail(); ?>
								</div>

								<!-- Title Post-->
								<div class="postTitle" style="padding:10px;height:70px;">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</div>

							</div>
						</div>
						<!-- End Post -->

					<?php endwhile; ?>
				<?php endif; ?>
				
				<?php if ($i == 0) : ?>
					<div class="alert alert-warning">NO FOUND WORKS</div>
				<?php endif; ?>
				<!-- End Posts -->

			</div>
			<!-- End Page -->

		</div>
	</div>
	<!-- End row4 (Content Main) -->
	
<!-- Get Footer -->
<?php get_footer(); ?>