<!-- Get Header -->
<?php get_header(); ?>

	<!-- Star row4 (Content Main) -->
	<div class="row4">
		<div class="container">

			<!-- Start Get Detail Post -->
			<!-- Start Posts -->
			<?php if(have_posts()): ?>
				<?php while(have_posts() ): the_post(); $i++; ?>
					
					<!-- Start Posts -->
					<div class="post postSingle">
						
						<!-- Image Page-->
						<div class="postImage">
							<?php the_post_thumbnail('medium'); ?>

							<!-- Title Post-->
							<div class="postTitle">
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</div>
						</div>

						<!-- Content Page-->
						<div class="postContent">
							<?php the_content(); ?>
						</div>

						<div class="comments">
							<?php comments_template(); ?>
						</div>

					</div>
					<!-- End Post -->
					
				<?php endwhile; ?>
			<?php endif; ?>
			<!-- End Get Detail Post -->

		</div>
	</div>
	<!-- End row4 (Content Main) -->
	
<!-- Get Footer -->
<?php get_footer(); ?>