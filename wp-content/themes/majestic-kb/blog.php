<!-- Get Header -->
<?php get_header(); ?>
<?php
/*
Template Name: Blog
*/
?>	

	<!-- Star row4 (Content Main) -->
	<div class="row4">
		<div class="container">

			<!-- Start Page -->
			<div class="page">

				<!-- Start Posts -->
				<?php $posts= get_posts('category_name=blog'); ?>
				<?php if ($posts) : ?>
					<?php foreach ( $posts as $post ) : ?>

						<!-- Set data post -->
						<?php setup_postdata($post); ?>

						<!-- Start Posts -->
						<div class="col-md-4">
							<div class="post">

								<!-- Image Post-->
								<div class="postImage">
									<?php the_post_thumbnail(); ?>
								</div>

								<!-- Title Post-->
								<div class="postTitle">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</div>

								<!-- Content Post-->
								<div class="postContent">
									<?php the_excerpt(); ?>
								</div>

							</div>
						</div>
						<!-- End Post -->

					<?php endforeach; ?>
				<?php endif; ?>
				<!-- End Posts -->

			</div>
			<!-- End Page -->

		</div>
	</div>
	<!-- End row4 (Content Main) -->
	
<!-- Get Footer -->
<?php get_footer(); ?>