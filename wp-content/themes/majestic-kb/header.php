<?php 
$options = get_options();
require_once('wp_bootstrap_navwalker.php');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	
	<title><?php bloginfo("name"); ?> | <?php bloginfo('description'); ?></title>

	<meta name="generator" content="Wordpress <?php bloginfo('version'); ?>" />
	
	<!-- Favicon -->
	<link rel="icon" type="image/png" href="<?php $options['favicon']; ?>">

	<!-- CSS Bootstrap -->
	<link rel="stylesheet" href="<?php echo CSS; ?>/bootstrap.min.css" />
	<link rel="stylesheet" href="<?php echo CSS; ?>/bootstrap-theme.min.css" />
	
	<!-- CSS Font Awesome -->
	<link rel="stylesheet" href="<?php echo wp_enqueue_script( 'script', get_template_directory_uri() . '/js/script.js', array ( 'jquery' ), 1.1, true); ?>/font-awesome.min.css" />
	<link rel="stylesheet" href="<?php echo CSS; ?>/font-awesome.min.css" />


	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	
	<!-- Testimonials -->
	<?php echo wp_enqueue_scripts('loadCssAndJs');?>

	<!-- CSS Primary -->
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" />
	<!-- Meta responsive -->
	<meta name="viewport" content="width=device-width" />

	<!-- Meta SEO -->
	<meta name="robots" content="noindex, follow">

	<style> .sub-menu li{ display: none !important; } </style>
</head>
<body <?php body_class(); ?>>
	
	<!-- Star row1 (Top Bar) -->
	<div class="row1 visible-lg">
		<div class="container">
			<div class="col-md-6 top-bar top-bar-left">Cantact Us in <?php echo $options['email-link']; ?></div>
			
			<!-- Socials Icons -->
			<div class="col-md-6 top-bar top-bar-right">
				<a href="<?php echo $options['instagram-link']; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a>
				<a href="<?php echo $options['facebook-link']; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
			</div>
			<!-- End Socials Icons -->
		</div>
	</div>
	<!-- End row1 (Top Bar) -->

	<!-- Star row2 (Header) -->
	<div class="row2">
		<div class="container">
			<div class="logo col-xs-12 col-md-4">
				<a href="<?php bloginfo("url"); ?>">
					<img src="<?php echo $options['logo']; ?>" alt="SEO Comment" class="img-logo">
				</a>
			</div>

			<!-- Start Contact phone -->
			<div class="contact-phone col-xs-12 col-md-8">
				<a href="tel:<?php echo $options['telefono-link']; ?>">
					<span class="hidden-xs">FREE ESTIMATE <i class="fa fa-phone"></i></span>
					<span class="phone"><?php echo $options['telefono']; ?></span>
				</a>
			<!-- End Contact phone -->
			</div>
		</div>
	</div>
	<!-- End row2 (Header) -->

	<!-- Star row3 (Primary Nav) -->
			<span id="top-link-block">
			    <a href="#top" class="well well-sm">
			        <i class="glyphicon glyphicon-chevron-up"></i>
			    </a>
			</span>
	<div class="row3">
		<div class="container">
			<div class="col-md-12">
				<?php //wp_nav_menu(array('menu' => 'Primary', 'container' => 'nav', 'container_id' => 'nav-main')); ?>
				<nav class="navbar navbar-default" role="navigation">
				  <div class="container-fluid">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				    </div>
				        <?php
				            wp_nav_menu( array(
				                'menu'              => 'primary',
				                'theme_location'    => 'primary',
				                'depth'             => 2,
				                'container'         => 'div',
				                'container_class'   => 'collapse navbar-collapse',
				        		'container_id'      => 'bs-example-navbar-collapse-1',
				                'menu_class'        => 'nav navbar-nav',
				                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
				                'walker'            => new wp_bootstrap_navwalker())
				            );
				        ?>
				    </div>
				</nav>
			</div>
			<style>
				.collapse, .navbar-collapse, .container-fluid{
					padding-left: 0px !important;
					padding-right: 0px !important;
					margin-left: 0px !important;
					margin-right: 0px !important;
					background: #0E1212;
				}
				.navbar-collapse{
					border-bottom: 5px solid #FAD435 !important;
				}
				.navbar,
				.navbar-default{
				    background: none !important;
				    border-radius: 0px !important;
				    border: 0px !important;
				    border-radius: 0px !important;
				    box-shadow: none !important;
				    -webkit-box-shadow: none !important;
				    box-shadow: none !important;
				    margin-bottom: 0px !important;
				}
				.navbar li.active a{
					background: #FAD435 !important;
					color: #000 !important;
				}
				.navbar li a{
					color: #FFF !important;
				}
				.navbar li a:hover{
					color: #FAD435 ;
				}
				.navbar .dropdown-menu{
					background: #000 !important;
					color: #000 !important;
					border-radius: 0px;
				}
				.navbar .dropdown-menu li a:hover{
					background: #FAD435 !important;
					color: #000 !important;
				}
				.dropdown-toggle.active{
					background: #FAD435 !important;
					color: #000 !important;	
				}
			</style>

		</div>
	</div>
	<!-- End row3 (Primary Nav) -->

	