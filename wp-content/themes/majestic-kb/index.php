<!-- Get Header -->
<?php get_header(); ?>
<?php $options = get_options(); ?>
	<!-- Star slider -->
	<div class="rowslider">
		<div class="container">
			<div class="col-md-12">
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
						<li data-target="#carousel-example-generic" data-slide-to="1"></li>
						<li data-target="#carousel-example-generic" data-slide-to="2"></li>
						<li data-target="#carousel-example-generic" data-slide-to="3"></li>
						<li data-target="#carousel-example-generic" data-slide-to="4"></li>
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<div class="item active item-slider">
							<img src="<?php echo $options['slider1']; ?>" class="img-slider" />
						</div>
						<div class="item item-slider">
							<img src="<?php echo $options['slider2']; ?>" class="img-slider" />
						</div>
						<div class="item item-slider">
							<img src="<?php echo $options['slider3']; ?>" class="img-slider" />
						</div>
						<div class="item item-slider">
							<img src="<?php echo $options['slider4']; ?>" class="img-slider" />
						</div>
						<div class="item item-slider">
							<img src="<?php echo $options['slider5']; ?>" class="img-slider" />
						</div>
					</div>

					<!-- Controls -->
					<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- End slider (Primary Nav) -->

	<!-- Star row4 (Content Main) -->
	<div class="row4">
		<div class="container">
			<div class="col-md-6">
				<?php echo $options['contenido-index-p1']; ?>
			</div>
			<div class="col-md-6">
				<?php echo $options['contenido-index-p2']; ?>
			</div>
		</div>
	</div>
	<!-- End row4 (Content Main) -->


<!-- Get Footer -->
<?php get_footer(); ?>