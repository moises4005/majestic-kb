<?php

/*##################################
	CUSTOM POST TYPE
################################## */

function register_wptestimo_post_type(){
	$labels=array(
					'name' => __('Testimonials', 'testimo'),
					'singular_name' => __('Testimonial', 'testimo'),
					'add_new' => __('Create Testimonial', 'testimo'),
					'add_new_item' => __('Add New Testimonial', 'testimo'),
					'edit_item' => __('Edit Testimonial', 'testimo'),
					'new_item' => __('New Testimonial', 'testimo'),
					'view_item' => __('View Testimonials', 'testimo'),
					'search_items' => __('Search Testimonials', 'testimo'),
					'not_found' => __('No Testimonials Found', 'testimo'),
					'not_found_in_trash' => __('No Testimonials Found in Trash', 'testimo')
					
				);
	$supports=array('title','editor','author','thumbnail','revisions');
		
	$args = array(
				'public' => true,
				'query_var' => true,
				'rewrite' => false,
				'show_in_nav_menus' => false,
				'publicly_queryable' => true,
				'has_archive' => false,
				'can_export' => true,
				'supports' => $supports,
				'exclude_from_search' => true,
				'menu_position' => 8,
				'singular_label' => __('Testimonial', 'testimo'),
				'labels' => $labels ,
				'capability_type' => 'post',
				'menu_icon' => 'dashicons-admin-users',
				'register_meta_box_cb' => 'wptestimo_info_create_metabox'
	);
	register_post_type('testimonial',$args);
	// flush_rewrite_rules();
}
add_action('init','register_wptestimo_post_type');
add_action('save_post','wptestimo_savemeta');
add_action('init','register_wptestimo_taxonomy');

//create custom taxonomy
function register_wptestimo_taxonomy(){
	$labels = array(
				'name' => __('Categories', 'testimo'),
				'singular_name' => __('Category', 'testimo'),
				'search_items' => __('Search Categories', 'testimo'),
				'popular_items' => __('Popular Category', 'testimo'),
				'all_items' => __('All Categorie', 'testimo'),
				'edit_item' => __('Edit Category', 'testimo'), 
				'update_item' => __('Update Category', 'testimo'),
				'add_new_item' =>  'Add New' ,
				'new_item_name' => __('New Category', 'testimo'),
				'menu_name' => __('Categories', 'testimo'),
			);
			
	$args = array(
						'hierarchical' => true,
						'labels' => $labels,
						'show_ui' => true,
						'query_var' => true,
						'show_tagcloud' => true,
						'rewrite' => true
					);
	//Register Taxonomy				
	register_taxonomy('testimonial-category','testimonial',$args);
}


//modify columns
function edit_wptestimo_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Name','testimo' ),
		'image' => __( 'Image','testimo' ),
		'content' => __( 'Testimonial','testimo' )
	);

	return $columns;
}
add_filter( 'manage_edit-testimonial_columns', 'edit_wptestimo_columns' ) ;

function manage_wptestimo_columns( $column, $post_id ) {
	global $post;

	switch( $column ) {

		case 'content' :
			echo get_the_content( $post_id ); 
		break;

		case 'image' :
			the_post_thumbnail(array(110,110));
		break;
		
		default :
			break;
	}
}
add_action( 'manage_testimonial_posts_custom_column', 'manage_wptestimo_columns', 10, 2 );

//create metabox
//Create Custom Metabox
function wptestimo_info_create_metabox(){
	add_meta_box('testimo-info', __('Testimonial Information', 'testimo'),'wptestimo_info_metabox','testimonial','normal','high');
}
function wptestimo_info_metabox($post){
	$meta = get_post_meta($post->ID, '_testimo_info', true);
	if(!empty($meta)){
		$meta = unserialize($meta);
	}
	?>
	<input type="hidden" name="wptestimo_info_nonce" value="<?php _e( wp_create_nonce(basename(__FILE__)) );?>" />
	<table class="form-table">
		<tbody>
			<tr>
				<th scope="row">
					<label for="testimo-tagline-fld"><?php _e('Tagline', 'testimo')?></label>
				</th>
				<td>
					<input type="text" id="testimo-tagline-fld" name="testimo[tagline]" class="widefat" value="<?php echo (isset($meta['tagline'])) ? $meta['tagline'] : '';?>"  />
				</td>
			</tr>
		</tbody>
	</table>
	<?php
}
function wptestimo_savemeta($post_id){
	if(isset( $_POST['wptestimo_info_nonce'] )){
		$info = serialize( $_POST['testimo'] );
		$info = strip_tags($info);
		update_post_meta($post_id, '_testimo_info', $info);
	}
}
?>