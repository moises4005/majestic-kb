/**
*
* VC Testimonial Addon
* URL: http://www.codecanyon.net/user/phpbits
* Version: 1.0
* Author: phpbits
* Author URL: http://www.codecanyon.net/user/phpbits
*
*/

// Utility
if ( typeof Object.create !== 'function' ) {
	Object.create = function( obj ) {
		function F() {};
		F.prototype = obj;
		return new F();
	};
}

(function( $, window, document, undefined ) {

	var construct_testimo = {
		init: function( options, elem ) {
			var self = this;

			self.elem = elem;
			self.$elem = $( elem );

			self.options = $.extend( {}, $.fn.testimo.options, options );
			

			self.display();
			self.doResponsive();
			self.doHover();
			self.doClick();

			jQuery(window).resize(function(){
				self.doResponsive();
			});
		},
		display: function(){
			var self = this;
			var layout = self.$elem.data('layout');
			// if('layout-2' == layout){
			// 	h = self.$elem.find('.testimo-image-wrapper').parent('.wpb_column').outerHeight();
			// 	// alert(h);
			// 	self.$elem.find('.testimo-content-wrapper').height( h );
			// }
		},
		doHover: function(){
			var self = this;
			var layout = self.$elem.data('layout');
			w = jQuery(window).width();
			// var uniqueid = self.$elem.data('uniqueid');
			self.$elem.find('.testimo-image-wrapper a').on('mouseenter',function($){
				target = jQuery(this).data('target');
				uniqueid = jQuery(this).data('uniqueid');
				
				// h = self.$elem.find('.testimo-content-wrapper #'+ target).height();
				// self.$elem.find('.testimo-content-wrapper').height(h);
				a_active = jQuery(this);
				self.$elem.find('.testimo-image-wrapper a').removeClass('testimo-active');
				if(!jQuery(this).hasClass('testimo-active')){
					
					jQuery.when( self.$elem.find('.testimo-content-wrapper .vc-testimo-content').fadeOut(300) )
                               .done(function() {
                        self.$elem.find('.testimo-content-wrapper #'+ target).fadeIn(250,function(){
							if('layout-1' == layout || 'layout-2' == layout){
								new_h = self.$elem.find('.testimo-content-wrapper #'+ target).innerHeight();
								self.$elem.find('.testimo-content-wrapper').height(new_h);
							}
							self.$elem.find('.testimo-content-wrapper .vc-testimo-content').removeClass('vc-testimo-content-active');
							self.$elem.find('.testimo-content-wrapper #'+ target).addClass('vc-testimo-content-active');
						});
					    
					});
                    a_active.addClass('testimo-active');

                    if('layout-3' == layout){
                    	self.$elem.find('.vc-testimo-tagline').hide();
                    	self.$elem.find('#vc-testimo-tagline-' + uniqueid).show();
                    }
				}
				
			});
		},
		doClick: function(){
			var self = this;
			self.$elem.find('.testimo-image-wrapper a').on('click',function(e){
				e.preventDefault();
			});
		},
		doResponsive: function(){
			var self = this;
			w = jQuery(window).width();
			items = self.$elem.data('responsive_items');
			count = 1;
			// console.log(w);
			if(w < 768){
				self.$elem.find('.testimo-image-wrapper .wpb_column').each(function(){
					if(count > parseInt(items) ){
						jQuery(this).hide();
					}else{
						jQuery(this).show();
					}
					count++;
				});
			}else{
				self.$elem.find('.testimo-image-wrapper .wpb_column').show();
			}
		}
	};

	$.fn.testimo = function( options ) {
		return this.each(function() {
			var testimo = Object.create( construct_testimo );
			
			testimo.init( options, this );

			$.data( this, 'testimo', testimo );
		});
	};

	$.fn.testimo.options = {
		layout : 'layout-1'
	};

})( jQuery, window, document );

jQuery(document).ready(function($){
	jQuery('.vc-testimo-addon').testimo();
});

jQuery(window).load(function($){
	testimo_fix();
});

jQuery(window).resize(function(){
	testimo_fix();
});

function testimo_fix(){
	w = jQuery(window).width();
	if(w > 767){
		jQuery('.vc-testimo-addon').each(function(){
			if(jQuery(this).hasClass('vc-testimo-layout-3')){
				h = jQuery(this).find('.testimo-image-wrapper').parent('.wpb_column').height();
				jQuery(this).find('.testimo-content-wrapper').height( h );
			}else if(jQuery(this).hasClass('vc-testimo-layout-1') || jQuery(this).hasClass('vc-testimo-layout-2')){
				h = jQuery(this).find('.vc-testimo-content-active').outerHeight();
				jQuery(this).find('.testimo-content-wrapper').height( h );
			}
		});
	}else{
		jQuery('.vc-testimo-addon').each(function(){
			if(jQuery(this).hasClass('vc-testimo-layout-3')){
				jQuery(this).find('.testimo-content-wrapper').height( 'auto' );
			}else if(jQuery(this).hasClass('vc-testimo-layout-1') || jQuery(this).hasClass('vc-testimo-layout-2') ){
				jQuery(this).find('.testimo-content-wrapper').height( 'auto' );
			}
		});
	}
}