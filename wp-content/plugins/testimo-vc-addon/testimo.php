<?php
/*
Plugin Name: Testimo - VC Testimonials Add-on for Wordpress (shared on wplocker.com)
Plugin URI: http://codecanyon.net/user/phpbits?ref=phpbits
Description: Perfect Testimonial Add on for your Visual Composer Plugin.
Version: 1.0
Author: phpbits
Author URI: http://codecanyon.net/user/phpbits?ref=phpbits
License: GPLv2 or later
*/

/*##################################
  REQUIRE
################################## */
require_once( dirname( __FILE__ ) . '/core/functions.post-type.php');

// don't load directly
if (!defined('ABSPATH')) die('-1');

class VCExtendAddonTestimo {
    function __construct() {
        // We safely integrate with VC with this hook
        add_action( 'init', array( $this, 'integrateWithVC' ) );
 
        // Use this when creating a shortcode addon
        add_shortcode( 'testimo', array( $this, 'renderMytestimo' ) );

        // Register CSS and JS
        add_action( 'wp_enqueue_scripts', array( $this, 'loadCssAndJs' ) );
    }
 
    public function integrateWithVC() {
        // Check if Visual Composer is installed
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            // Display notice that Visual Compser is required
            add_action('admin_notices', array( $this, 'showVcVersionNotice' ));
            return;
        }
 
        /*
        Add your Visual Composer logic here.
        Lets call vc_map function to "register" our custom shortcode within Visual Composer interface.

        More info: http://kb.wpbakery.com/index.php?title=Vc_map
        */
        $categories = get_terms( 'testimonial-category', array(
          'orderby'    => 'title',
          'hide_empty' => 0,
         ) );
        $tax = array();
        $tax[] = 'All';
        if ( ! empty( $categories ) && ! is_wp_error( $categories ) ) {
          foreach ($categories as $key => $value) {
            $tax[] = $value->name;
          }
        }
        vc_map( array(
            "name" => __("Testimonials", 'vc_extend'),
            "description" => __("Display Testimonials on your posts/page", 'vc_extend'),
            "base" => "testimo",
            "class" => "",
            "controls" => "full",
            "icon" => plugins_url('assets/images/icon.png', __FILE__), // or css class name which you can reffer in your css file later. Example: "vc_extend_my_class"
            "category" => __('Content', 'js_composer'),
            //'admin_enqueue_js' => array(plugins_url('assets/vc_extend.js', __FILE__)), // This will load js file in the VC backend editor
            //'admin_enqueue_css' => array(plugins_url('assets/vc_extend_admin.css', __FILE__)), // This will load css file in the VC backend editor
            "params" => array(
                array(
                  "type" => "textfield",
                  "holder" => '',
                  "class" => "testimo-title",
                  "heading" => __("Title", 'vc_extend'),
                  "param_name" => "title",
                  "value" => __("Testimonials", 'vc_extend'),
                  "description" => __("Enter text which will be used as title. Leave blank if no title is needed.", 'vc_extend'),
                  'admin_label' => true
              ),
              array(
                  "type" => "dropdown",
                  "holder" => "",
                  "class" => "",
                  "heading" => __("Category", 'vc_extend'),
                  "param_name" => "category",
                  "value" => $tax, //Default Red color
                  "description" => __("Select Testimonial Category you want to display.", 'vc_extend'),
                  'admin_label' => true
              ),
              array(
                  "type" => "dropdown",
                  "holder" => "",
                  "class" => "",
                  "heading" => __("Orderby", 'vc_extend'),
                  "param_name" => "orderby",
                  "value" => array('Default','ID','Title'), //Default Red color
                  "description" => __("Select ordering options", 'vc_extend'),
                  'admin_label' => true
              ),
              array(
                  "type" => "dropdown",
                  "holder" => "",
                  "class" => "",
                  "heading" => __("Order", 'vc_extend'),
                  "param_name" => "order",
                  "value" => array('Default','Ascending','Descending'), //Default Red color
                  "description" => __("Select ordering options", 'vc_extend'),
                  'admin_label' => true
              ),
              array(
                  "type" => "dropdown",
                  "holder" => "",
                  "class" => "",
                  "heading" => __("Columns", 'vc_extend'),
                  "param_name" => "columns",
                  "value" => array(1,2,3,4,6,8,10,12), //Default Red color
                  "description" => __("Choose number of columns to be displayed", 'vc_extend'),
                  'admin_label' => true
              ),
              array(
                  "type" => "dropdown",
                  "holder" => "",
                  "class" => "",
                  "heading" => __("Responsive Columns", 'vc_extend'),
                  "param_name" => "columns_responsive",
                  "value" => array(1,2,3,4,6), //Default Red color
                  "description" => __("Choose number of columns to be displayed on Mobile Version", 'vc_extend'),
                  'admin_label' => true
              ),
              array(
                  "type" => "dropdown",
                  "holder" => "",
                  "class" => "",
                  "heading" => __("Alignment", 'vc_extend'),
                  "param_name" => "alignment",
                  "value" => array('Left' => 'Left', 'Center' => 'Center', 'Right' => 'Right'),
                  "description" => __("Select Default Alignment", 'vc_extend'),
                  'admin_label' => true
              ),
              array(
                  "type" => "textfield",
                  "holder" => "",
                  "class" => "",
                  "heading" => __("Image Size", 'vc_extend'),
                  "param_name" => "size",
                  "value" => __("", 'vc_extend'),
                  "description" => __('Enter image size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size.', 'vc_extend'),
                  'admin_label' => true
              ),
              array(
                  "type" => "textfield",
                  "holder" => "",
                  "class" => "",
                  "heading" => __("Number of Items", 'vc_extend'),
                  "param_name" => "items",
                  "value" => 20,
                  "description" => __('Number of testimonials items displayed on Desktop Version', 'vc_extend'),
                  'admin_label' => true
              ),
              array(
                  "type" => "textfield",
                  "holder" => "",
                  "class" => "",
                  "heading" => __("Number of Items (Responsive)", 'vc_extend'),
                  "param_name" => "items_responsive",
                  "value" => 12,
                  "description" => __('Number of testimonials items displayed on Mobile and Tablet Version. This can\'t be greater than number of items on desktop.', 'vc_extend'),
                  'admin_label' => true
              ),
              array(
                  "type" => "textfield",
                  "holder" => "",
                  "class" => "",
                  "heading" => __("Font Size", 'vc_extend'),
                  "param_name" => "font_size",
                  "value" => 22,
                  "description" => __('Testimonial Content Font Size', 'vc_extend'),
                  'admin_label' => true
              ),
              array(
                  "type" => "colorpicker",
                  "holder" => "",
                  "class" => "",
                  "heading" => __("Primary Background Color", 'vc_extend'),
                  "param_name" => "bgcolor",
                  "value" => '#0274be',
                  "description" => __('Background and hover animation Color', 'vc_extend'),
                  'admin_label' => true
              ),
              array(
                  "type" => "colorpicker",
                  "holder" => "",
                  "class" => "",
                  "heading" => __("Secondary Background Color", 'vc_extend'),
                  "param_name" => "bgcolor2",
                  "value" => '#333333',
                  "description" => __('Some Layout requires secondary background color for better visual designs.', 'vc_extend'),
                  'admin_label' => true
              ),
              array(
                  "type" => "colorpicker",
                  "holder" => "",
                  "class" => "",
                  "heading" => __("Text Color", 'vc_extend'),
                  "param_name" => "color",
                  "value" => '',
                  "description" => __('Testimonial Content Text Color', 'vc_extend'),
                  'admin_label' => true
              ),
              array(
                  "type" => "textfield",
                  "holder" => "",
                  "class" => "",
                  "heading" => __("Separator", 'vc_extend'),
                  "param_name" => "separator",
                  "value" => '-',
                  "description" => __('Name and Tagline Separator Text', 'vc_extend'),
                  'admin_label' => true
              ),
              array(
                  "type" => "dropdown",
                  "holder" => "",
                  "class" => "",
                  "heading" => __("Layout", 'vc_extend'),
                  "param_name" => "layout",
                  "value" => array('Layout-1', 'Layout-2', 'Layout-3'),
                  "description" => __("Select Layout and Styles. Below are the visual representation of the available styles: <br /> 
                    <div class='vc_row wpb_edit_form_elements'>
                      <div class='vc_col-sm-4 vc_column' style='padding:8px;'><img src='". plugins_url('assets/images/layout-1.jpg', __FILE__) ."' /></div>
                      <div class='vc_col-sm-4 vc_column' style='padding:8px;'><img src='". plugins_url('assets/images/layout-2.jpg', __FILE__) ."' /></div>
                      <div class='vc_col-sm-4 vc_column' style='padding:8px;'><img src='". plugins_url('assets/images/layout-3.jpg', __FILE__) ."' /></div>
                    </div>", 'vc_extend'),
                  'admin_label' => true
              ),
            )
        ) );
    }
    
    /*
    Shortcode logic how it should be rendered
    */
    public function renderMytestimo( $atts, $content = null ) {
      extract( shortcode_atts( array(
        'title' => null,
        'category' => 'All',
        'columns' => 6,
        'columns_responsive' => 6,
        'alignment' => 'Center',
        'size' => 'thumbnail',
        'items' => 20,
        'items_responsive' => 12,
        'layout' => 'layout-1',
        'separator' => '-',
        'bgcolor' => '#0274be',
        'bgcolor2' => '#222222',
        'color' => null,
        'font_size' => 22,
        'orderby' => '',
        'order' => '',
      ), $atts ) );
      $layout = strtolower($layout);
      $content = wpb_js_remove_wpautop($content, true); // fix unclosed/unwanted paragraph tags in $content
      $count = 0;
      $image_div = '';
      $content_div = '';
      $unique = uniqid();
      $alignment = strtolower($alignment);
      $styles = '<style>';
      $tagline = '<div class="testimo-tagline-container">';
      
      $columns_responsive = 12 / $columns_responsive;
      $sizes = explode('x', $size);
      if(isset($sizes[1]) && intval($sizes[1]) > 0){
        $size = array($sizes[0], $sizes[1]);
      }
      if(in_array($columns, array(8,10))){
        $columns = 'testimo-' . $columns;
      }else{
        $columns = 12 / $columns;
      }
      //prepare wp-query args
      $args = array(
            'post_type' => 'testimonial',
            'post_status' =>  'publish',
          );

      if('All' != $category){
        $term = get_term_by('name', $category, 'testimonial-category');
        $args['tax_query'] = array(
                    array(
                      'taxonomy' => 'testimonial-category',
                      'field'    => 'id',
                      'terms'    => $term->term_id,
                    )
          );
      }

      if(!empty($items)){
        $args['posts_per_page'] = intval($items);
      }

      if(!empty($orderby) && 'Default' != $orderby){
        $args['orderby'] = strtolower($orderby);
      }

      if(!empty($order) && 'Default' != $order){
        switch ($order) {
          case 'Ascending':
            $args['order'] = 'ASC';
            break;

          case 'Descending':
            $args['order'] = 'DESC';
            break;
          
          default:
            # code...
            break;
        }
      }
      // print_r($args);
      $query = new WP_Query( $args );

      //check if there are testimonials
      if($query->have_posts()){
        $output = "<div class='vc_txt_align_{$alignment} vc-testimo-addon vc-testimo-{$layout}' id='testimo-{$unique}' data-items='{$items}' data-responsive_items='{$items_responsive}' data-layout='{$layout}' >";
        if(!empty($title)){
          $output .= '<h2>'. $title .'</h2>';
        }
        $output .= "<div class='vc_row wpb_row vc_row-fluid'>";
        //start loop
        while ($query->have_posts()) { $query->the_post(); 
          if(has_post_thumbnail( get_the_ID() )){
            $meta = get_post_meta(get_the_ID(), '_testimo_info', true);
            if(!empty($meta)){
              $meta = unserialize($meta);
            }
            //reset tagline
            if(in_array($layout, array('layout-1', 'layout-2'))){
              $tagline  = '';
            }
            $tagline .= '<div class="vc-testimo-tagline';
            if($count == 0){
                $tagline .= ' vc-testimo-tagline-active';
            }
            $tagline .= '" id="vc-testimo-tagline-'. $unique .'-'. get_the_ID() .'">';
            $tagline .= get_the_title();
            if(isset($meta['tagline']) && !empty($meta['tagline'])){
              $tagline .= ' <span>'. $separator .' '. $meta['tagline'] .'</span>';
            }
            $tagline .= '</div>';

            $image_div .= "<div class='vc_col-sm-{$columns} vc_col-xs-{$columns_responsive} wpb_column vc_column_container'>";
              $image_div .= '<a href="#" data-target="testimo-'. $unique .'-'. get_the_ID() .'" data-uniqueid="'. $unique . '-' . get_the_ID() .'" class="';
              if($count == 0){
                $image_div .= 'testimo-active';
              }
              $image_div .= '" >';
                $image_div .= get_the_post_thumbnail( get_the_ID(), $size );
              $image_div .= "</a>";
            $image_div .= "</div>";

            //add content divs
            $content_div .= '<div class="vc-testimo-content ';
            if($count == 0){
              $content_div .= 'vc-testimo-content-active';
            }
            $content_div .= '" id="testimo-'. $unique .'-'. get_the_ID() .'">';
            $content_div .= get_the_content();
            
            
            if(in_array($layout, array('layout-1', 'layout-2'))){
              $content_div .= $tagline;
            }
            $content_div .= '</div>';
            $count++;
          }
        } //endwhile loop
        $tagline .= '</div>';

        //merge to output
        switch ($layout) {
          case 'layout-1':
            $output .= '<div class="testimo-image-wrapper">' . $image_div . '</div>';
            $output .= '<div class="testimo-content-wrapper">' .$content_div . '</div>';
            break;

          case 'layout-2':
            $output .= '<div class="testimo-content-wrapper">' .$content_div . '</div>';
            $output .= '<div class="testimo-image-wrapper">' . $image_div . '</div>';
            break;

          case 'layout-3':
            $output .= '<div class="vc_col-sm-8 vc_col-xs-12 wpb_column vc_column_container testimo-right">';
            $output .= '<div class="testimo-image-wrapper">' . $image_div;
            $output .= $tagline;
            $output .= '<div class="testimo-clear"></div></div>';
            $output .= "</div>";

            $output .= '<div class="vc_col-sm-4 vc_col-xs-12 wpb_column vc_column_container testimo-left">';
            $output .= '<div class="testimo-content-wrapper">' .$content_div . '</div>';
            $output .= "</div>";
            break;
          
          default:
            # code...
            break;
        }

        switch ($layout) {
          case 'layout-1':
            $styles .= '.vc-testimo-addon#testimo-'. $unique .' .vc_row .testimo-image-wrapper .wpb_column a{ background:'. $bgcolor .'; }';
            $styles .= '.vc-testimo-addon#testimo-'. $unique .' .vc_row .testimo-content-wrapper .vc-testimo-tagline{ font-size:'. ( intval($font_size) - 2 ) .'px; }';
            break;

          case 'layout-2':
            $styles .= '.vc-testimo-addon#testimo-'. $unique .' .vc_row .testimo-image-wrapper .wpb_column a{ background:'. $bgcolor .'; }';
            $styles .= '.vc-testimo-addon#testimo-'. $unique .' .vc_row .testimo-content-wrapper .vc-testimo-tagline{ font-size:'. ( intval($font_size) - 2 ) .'px; }';
            break;

          case 'layout-3':
             $styles .= '.vc-testimo-addon#testimo-'. $unique .' .vc_row, .vc-testimo-addon#testimo-'. $unique .' .vc_row .testimo-image-wrapper .wpb_column a{ background:'. $bgcolor .'; }';
             $styles .= '.vc-testimo-addon#testimo-'. $unique .' .vc_row .testimo-tagline-container{ background:'. $bgcolor2 .'; }';
             $styles .= '.vc-testimo-addon#testimo-'. $unique .' .vc_row .testimo-tagline-container .vc-testimo-tagline{ font-size:'. intval($font_size) .'px; }';
            break;
          
          default:
            # code...
            break;
        }

        $styles .= '.vc-testimo-addon#testimo-'. $unique .' .vc_row .testimo-content-wrapper .vc-testimo-content, .vc-testimo-addon#testimo-'. $unique .' .vc_row .testimo-content-wrapper .vc-testimo-content p{ color:'. $color .'; }';
        $styles .= '.vc-testimo-addon#testimo-'. $unique .' .vc_row .testimo-content-wrapper .vc-testimo-content, .vc-testimo-addon#testimo-'. $unique .' .vc_row .testimo-content-wrapper .vc-testimo-content p{ font-size:'. intval($font_size) .'px; }';

        $output .= "</div>";
        $output .= "<div class='testimo-clear'></div>";
        $output .= "</div>";

        wp_reset_postdata();
      } //end $query->have_posts()
     
      $styles .= '</style>';
      $output .= $styles;
      return $output;
    }

    /*
    Load plugin css and javascript files which you may need on front end of your site
    */
    public function loadCssAndJs() {
      wp_register_style( 'testimo_extend_style', plugins_url('assets/css/testimo.css', __FILE__) );
      wp_enqueue_style( 'testimo_extend_style' );

      // If you need any javascript files on front end, here is how you can load them.
      wp_enqueue_script( 'testimo_extend_js', plugins_url('assets/js/testimo.js', __FILE__), array('jquery') );
    }

    /*
    Show notice if your plugin is activated but Visual Composer is not
    */
    public function showVcVersionNotice() {
        $plugin_data = get_plugin_data(__FILE__);
        echo '
        <div class="updated">
          <p>'.sprintf(__('<strong>%s</strong> requires <strong><a href="http://codecanyon.net/item/visual-composer-page-builder-for-wordpress/242431?ref=phpbits" target="_blank">Visual Composer</a></strong> plugin to be installed and activated on your site.', 'vc_extend'), $plugin_data['Name']).'</p>
        </div>';
    }
}
// Finally initialize code
new VCExtendAddonTestimo();